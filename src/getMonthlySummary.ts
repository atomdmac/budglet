import {
  BudgetItem,
  CategorySummary,
  MonthlySummary,
  Transaction,
  TransactionType,
  UUID,
} from "./types";

export async function getMonthlySummary(
  transactions: Transaction[],
  budgetItems: BudgetItem[]
): Promise<MonthlySummary> {
  const summary: MonthlySummary = {
    spent: 0,
    budgeted: 0,
    categories: {},
  };

  function getSummary(categoryId: UUID): CategorySummary {
    return (
      summary.categories[categoryId] || {
        categoryId,
        spent: 0,
        budgeted: 0,
      }
    );
  }

  // TODO: Do not include `Income` or `Starting Balance` types in `spent` output.
  transactions.forEach((transaction) => {
    const { categoryId, amount } = transaction;
    const current = getSummary(categoryId);
    const valance = transaction.type == TransactionType.Debit ? 1 : -1;
    current.spent += amount * valance;
    summary.spent += amount;
    summary.categories[categoryId] = current;
  });

  budgetItems.forEach((budgetItem) => {
    const { categoryId } = budgetItem;
    const current = getSummary(categoryId);
    current.budgeted += budgetItem.amount;
    summary.budgeted += budgetItem.amount;
    summary.categories[categoryId] = current;
  });

  return summary;
}
