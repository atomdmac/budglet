import { Transaction, TransactionType } from "./types";

export default async function sumTransactions(
  transactions: Transaction[]
): Promise<number> {
  function reduceTransactions(
    total: number,
    curTransaction: Transaction
  ): number {
    const valance = curTransaction.type == TransactionType.Credit ? 1 : -1;
    return total + curTransaction.amount * valance;
  }

  return transactions.reduce(reduceTransactions, 0);
}
