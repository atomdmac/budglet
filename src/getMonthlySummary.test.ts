import { BudgetItem, Transaction, TransactionType } from "./types";
import { getMonthlySummary } from "./getMonthlySummary";

describe("getMonthlySummary", function () {
  const categoryId1 = "c1";
  const categoryId2 = "c2";

  const transactions: Transaction[] = [
    {
      amount: 10,
      categoryId: categoryId1,
      date: new Date(),
      id: "fake-id-1",
      memo: "Fake Transaction 1",
      payee: {
        id: "fake-payee-id",
        name: "Fake Payee 1",
      },
      type: TransactionType.Debit,
    },
    {
      amount: 20,
      categoryId: categoryId2,
      date: new Date(),
      id: "fake-id-2",
      memo: "Fake Transaction 1",
      payee: {
        id: "fake-payee-id",
        name: "Fake Payee 1",
      },
      type: TransactionType.Debit,
    },
    {
      amount: 10,
      categoryId: categoryId2,
      date: new Date(),
      id: "fake-id-3",
      memo: "Fake Transaction 3",
      payee: {
        id: "fake-payee-id",
        name: "Fake Payee 1",
      },
      type: TransactionType.Debit,
    },
    {
      amount: 5,
      categoryId: categoryId2,
      date: new Date(),
      id: "fake-id-4",
      memo: "Fake Transaction 4",
      payee: {
        id: "fake-payee-id",
        name: "Fake Payee 1",
      },
      type: TransactionType.Credit,
    },
  ];

  const budgetItems: BudgetItem[] = [
    {
      categoryId: categoryId1,
      amount: 5,
    },
    {
      categoryId: categoryId2,
      amount: 5,
    },
  ];
  it("should return a MonthlySummary object", async function () {
    const summary = await getMonthlySummary(transactions, budgetItems);

    expect(summary.spent).toEqual(35);
    expect(summary.budgeted).toEqual(10);

    expect(summary.categories["c1"]).toMatchObject({
      categoryId: categoryId1,
      spent: 10,
      budgeted: 5,
    });
    expect(summary.categories["c2"]).toMatchObject({
      categoryId: categoryId2,
      spent: 25,
      budgeted: 5,
    });
  });
});
