import sumTransactions from "./sumTransactions";
import { Transaction, TransactionType } from "./types";

describe("sumTransactions", function () {
  const transactions: Transaction[] = [
    {
      amount: 10,
      categoryId: "fake-id-1",
      date: new Date(),
      id: "fake-id-1",
      memo: "Fake Transaction 1",
      payee: {
        id: "fake-payee-id",
        name: "Fake Payee 1",
      },
      type: TransactionType.Credit,
    },
    {
      amount: 20,
      categoryId: "fake-id-2",
      date: new Date(),
      id: "fake-id-2",
      memo: "Fake Transaction 1",
      payee: {
        id: "fake-payee-id",
        name: "Fake Payee 1",
      },
      type: TransactionType.Credit,
    },
    {
      amount: 5,
      categoryId: "fake-id-3",
      date: new Date(),
      id: "fake-id-3",
      memo: "Fake Transaction 3",
      payee: {
        id: "fake-payee-id",
        name: "Fake Payee 1",
      },
      type: TransactionType.Debit,
    },
  ];

  it("Should add up amounts of transactions (accounting for debit vs. credit)", async function () {
    const result = await sumTransactions(transactions);
    expect(result).toEqual(25);
  });
});
