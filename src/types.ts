export type UUID = string;

export type Category = {
  id: UUID;
  name: string;
};

export enum AccountType {
  Cash,
  Checking,
  Credit,
  Investment,
  Mortgage,
  Other,
  Saving,
}

export enum TransactionType {
  Credit = "Credit",
  Debit = "Debit",
}

/**
 * A source of money or credit.
 */
export interface Account {
  id: UUID;
  name: string;
  type: AccountType;
}

/**
 * The counter party for a transaction.
 */
export interface Payee {
  id: UUID;
  name: string;
}

/**
 * An occurance of money entering or leaving an Account.
 */
export interface Transaction {
  amount: number;
  categoryId: UUID;
  date: Date;
  id: UUID;
  memo: string;
  payee: Payee;
  type: TransactionType;
}

/**
 * How much money has been put toward a particular category for a given month.
 */
export interface BudgetItem {
  categoryId: UUID;
  amount: number;
}

/**
 * Total total amount spent and budgeted for a given Category.
 */
export interface CategorySummary {
  categoryId: UUID;
  spent: number;
  budgeted: number;
}

/**
 * All of the items required to define a budget for a single month.
 */
export interface MonthlyBudget {
  month: number;
  year: number;
  transactions: Transaction[];
  budgetItems: BudgetItem[];
}

/**
 * The total amount spent and budgeted for a given month.
 */
export interface MonthlySummary {
  spent: number;
  budgeted: number;
  categories: {
    [key: string]: CategorySummary;
  };
}
